﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Drawing;



namespace MK_DealerPriceEmail_Console
{
    class Program
    {
        static void Main(string[] args)
        {

            List<DPSEmailList> EmailList;

           Console.WriteLine(CheckPricesApproved()+" Prices Approved");
           Console.WriteLine(CheckForNullIG()+" Missing Item groups");
           //ProcessMailLabels();
          
          //SSRSReport();
           // Console.WriteLine(GetPriceRowCount());
            //CreateDealerPriceSheetPDF();
          EmailList =  GetEmails();

         Console.WriteLine("");

        }
        public static void CreateDealerPriceSheetPDF()
        {
            CreateSSRSReport(@"/Operations/MK_DealerPriceSheetCover", @"C:\Users\wlin\Newfolder\New folder\Test\DealerPriceSheetCover.pdf");
            CreateSSRSReport(@"/Operations/MK_DealerPriceSheet_SSRS", @"C:\Users\wlin\Newfolder\New folder\Test\DealerPriceSheetBody.pdf");
            MergePDF(@"C:\Users\wlin\Newfolder\New folder\Test\DealerPriceSheetCover.pdf", @"C:\Users\wlin\Newfolder\New folder\Test\DealerPriceSheetBody.pdf", @"C:\Users\wlin\Newfolder\New folder\Test\DealerPriceSheet.pdf");
        }
      static  public int CheckPricesApproved()
        {
            int priceChanges;
            string sql2ConnString = "Data Source=Sql2;Initial Catalog=KencoveSql;Integrated Security=True";
            SqlConnection sql2Conn = new SqlConnection(sql2ConnString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = "EXEC MK_LogPrices_ToDo_Console";
            cmd.CommandType = CommandType.Text;
           
            cmd.Connection = sql2Conn;
            //cmd.Parameters.Add("@numberToDo", SqlDbType.Int).Value = 1;
            sql2Conn.Open();
           
            //cmd.ExecuteNonQuery();
           reader= cmd.ExecuteReader();
          if(reader.Read())
          {
              priceChanges = reader.GetInt32(0);
          }
          else
          {
              priceChanges = 0;
          }
          reader.Close();
          sql2Conn.Close();

             return priceChanges;
        }

      static public int CheckForNullIG()
      {
          int val;
          string sql2ConnString = "Data Source=Sql2;Initial Catalog=KencoveSql;Integrated Security=True";
          SqlConnection sql2Conn = new SqlConnection(sql2ConnString);
          SqlCommand cmd = new SqlCommand();
          SqlDataReader reader;
          cmd.CommandText = "SELECT COUNT(*) Count FROM CI_Item WHERE ISNULL(UDF_IG1,'')='' AND UDF_VISIBILITY > 3";
          cmd.CommandType = CommandType.Text;
          cmd.CommandType = CommandType.Text;

          cmd.Connection = sql2Conn;
          //cmd.Parameters.Add("@numberToDo", SqlDbType.Int).Value = 1;
          sql2Conn.Open();

          //cmd.ExecuteNonQuery();
          reader = cmd.ExecuteReader();
          if (reader.Read())
          {
              val = reader.GetInt32(0);
          }
          else
          {
              val = 0;
          }

          reader.Close();
          sql2Conn.Close();
          return val;
      }

      static public List<DPSEmailList> GetEmails()
      {
          DPSEmailList current;
          List<DPSEmailList> list = new List<DPSEmailList>();
         
          int count =0;
          string sql2ConnString = "Data Source=Sql2;Initial Catalog=KencoveSql;Integrated Security=True";
          SqlConnection sql2Conn = new SqlConnection(sql2ConnString);
          SqlCommand cmd = new SqlCommand();
          SqlDataReader reader;
          cmd.CommandText = "dbo.MK_DealerPriceSheet_Log";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.Add("@log", SqlDbType.Bit);
          cmd.Parameters.Add("@count", SqlDbType.Int).Direction=ParameterDirection.Output;

          cmd.Parameters["@log"].Value = 0;
          cmd.Connection = sql2Conn;
          //cmd.Parameters.Add("@numberToDo", SqlDbType.Int).Value = 1;
          sql2Conn.Open();
          int val;
          //cmd.ExecuteNonQuery();
          reader = cmd.ExecuteReader();
          Object[] values = new Object[reader.FieldCount];
            while(reader.HasRows)  //&& count < 477
            {
                if (reader.Read() )
                {
                    val = reader.GetValues(values);
                    current.Name = (values[0] == DBNull.Value) ? string.Empty: (string)values[0]; //reader.GetString(0);

                    current.Freq = (values[1] == DBNull.Value) ? string.Empty : (string)values[1];//reader.GetString(1);
                    current.Email = (values[2] == DBNull.Value) ? string.Empty : (string)values[2];//reader.GetString(2);
                    current.CustomerNo = (values[3] == DBNull.Value) ? string.Empty : (string)values[3];// reader.GetString(3);
                    current.PriceLevel = (values[4] == DBNull.Value) ? string.Empty : (string)values[4];//reader.GetString(4);
                    current.AttachSpeadsheet = (values[5] == DBNull.Value) ? 0: (int)values[5];//reader.GetInt32(5);
                    current.AttachPDF = (values[6] == DBNull.Value) ? 0 : (int)values[6];//reader.GetInt32(6);
                    current.Fax = (values[7] == DBNull.Value) ? string.Empty : (string)values[7];//reader.GetString(7);
                    current.LastSent = (values[8] == DBNull.Value) ? default(DateTime) : (DateTime)values[8];//reader.GetDateTime(8);
                    current.PriceChanges = (values[9] == DBNull.Value) ? 0 : (int)values[9];//reader.GetInt32(9);
                    current.SendAfter = (values[10] == DBNull.Value) ? default(DateTime) : (DateTime)values[10];//reader.GetDateTime(10);
                    current.Email2 = (values[11] == DBNull.Value) ? string.Empty : (string)values[11];//reader.GetString(11);
                    current.Inactive = (values[12] == DBNull.Value) ? true : (Boolean)values[12]; //reader.GetBoolean(12);

                    list.Add(current);
                    count++;
                    Console.WriteLine(count);
                }
                else
                {
                    break;
                }
            }
          //foreach(DPSEmailList row in list)
          //{
          //    Console.Write("Name: {0} ", row.Name);
          //    Console.Write("Email: {0}", row.Email);
          //    Console.WriteLine("Email: {0}", row.LastSent);
          //}
          //if (reader.Read())
          //{
          //    strVal = reader.GetString(0);

          //}
          //else
          //{
          //    val = 0;
          //}
          //Console.WriteLine(cmd.Parameters["@count"].Value);
         
          reader.Close();
          sql2Conn.Close();
          return list;
      }
    static public void ProcessMailLabels()
      {
          int rowsAffected;
          string sql2ConnString = "Data Source=Sql2;Initial Catalog=KencoveSql;Integrated Security=True";
          SqlConnection sql2Conn = new SqlConnection(sql2ConnString);
          SqlCommand cmd = new SqlCommand();
          SqlDataReader reader;
          cmd.CommandText = "EXEC dbo.MK_DealerPriceSheet_LogMail";
          cmd.CommandType = CommandType.Text;
          cmd.CommandType = CommandType.Text;
          cmd.Connection = sql2Conn;
          sql2Conn.Open();
          rowsAffected= cmd.ExecuteNonQuery();
          sql2Conn.Close();
      }
      static public int GetPriceRowCount()
      {
          int val;
          string sql2ConnString = "Data Source=Sql2;Initial Catalog=KencoveSql;Integrated Security=True";
          SqlConnection sql2Conn = new SqlConnection(sql2ConnString);
          SqlCommand cmd = new SqlCommand();
          SqlDataReader reader;
          cmd.CommandText = "EXEC MK_DealerPriceSheet_COUNT_Console";
          cmd.CommandType = CommandType.Text;
          cmd.CommandType = CommandType.Text;

          cmd.Connection = sql2Conn;
          //cmd.Parameters.Add("@numberToDo", SqlDbType.Int).Value = 1;
          sql2Conn.Open();

          //cmd.ExecuteNonQuery();
          reader = cmd.ExecuteReader();
          if (reader.Read())
          {
              val = reader.GetInt32(0);
          }
          else
          {
              val = 0;
          }

          reader.Close();
          sql2Conn.Close();
          return val;
      }
      //  public static void CrytsalReport()
      //{
      //      ReportDocument cryRpt
      //}
      public static void SSRSReport()
      {
          ReportServices2005.ReportingService2005 rs = new ReportServices2005.ReportingService2005();
          RE2005.ReportExecutionService rsExec = new RE2005.ReportExecutionService();

          rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
          rsExec.Credentials = System.Net.CredentialCache.DefaultCredentials;

          rs.Url = "http://sqlent/reportserver/reportservice2005.asmx";
          rsExec.Url = "http://sqlent/reportserver/reportexecution2005.asmx";

          string historyID = null;
          string deviceInfo = null;
          string format = "PDF";
          Byte[] results;
          string encoding = String.Empty;
          string mimeType = String.Empty;
          string extension = String.Empty;
          RE2005.Warning[] warnings = null;
          string[] streamIDs = null;
            //KC_OrderStatus
          string fileName = @"C:\Users\wlin\Newfolder\New folder\Test\OrderStatus1.pdf";
          // Name of the report - Please note this is not the RDL file.
          string _reportName = @"/Operations/MK_DealerPriceSheet_SSRS";
          string _historyID = null;
          bool _forRendering = false;
          ReportServices2005.ParameterValue[] _values = null;
          ReportServices2005.DataSourceCredentials[] _credentials = null;
          ReportServices2005.ReportParameter[] _parameters = null;

          try
          {
              _parameters = rs.GetReportParameters(_reportName, _historyID, _forRendering, _values, _credentials);
              RE2005.ExecutionInfo ei = rsExec.LoadReport(_reportName, historyID);
              RE2005.ParameterValue[] parameters = new RE2005.ParameterValue[1];

              if (_parameters.Length > 0)
              {
                  parameters[0] = new RE2005.ParameterValue();
                  parameters[0].Label = "userlogon";
                  parameters[0].Name = "userlogon";
                  parameters[0].Value = "ccastaldo";
              }
              rsExec.SetExecutionParameters(parameters, "en-us");
              results = rsExec.Render(format, deviceInfo,
               out extension, out encoding,
               out mimeType, out warnings, out streamIDs);
              using (FileStream stream = File.OpenWrite(fileName))
              {
                  stream.Write(results, 0, results.Length);
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }

        
         
      }
      public static void CreateSSRSReport(string reportName, string pdfName)
      {
          ReportServices2005.ReportingService2005 rs = new ReportServices2005.ReportingService2005();
          RE2005.ReportExecutionService rsExec = new RE2005.ReportExecutionService();

          rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
          rsExec.Credentials = System.Net.CredentialCache.DefaultCredentials;

          rs.Url = "http://sqlent/reportserver/reportservice2005.asmx";
          rsExec.Url = "http://sqlent/reportserver/reportexecution2005.asmx";

          string historyID = null;
          string deviceInfo = null;
          string format = "PDF";
          Byte[] results;
          string encoding = String.Empty;
          string mimeType = String.Empty;
          string extension = String.Empty;
          RE2005.Warning[] warnings = null;
          string[] streamIDs = null;
          //KC_OrderStatus
          string fileName = pdfName;//@"C:\Users\wlin\Newfolder\New folder\Test\OrderStatus1.pdf";
          // Name of the report - Please note this is not the RDL file.
          string _reportName = reportName;// @"/Operations/MK_DealerPriceSheet_SSRS";
          string _historyID = null;
          bool _forRendering = false;
          ReportServices2005.ParameterValue[] _values = null;
          ReportServices2005.DataSourceCredentials[] _credentials = null;
          ReportServices2005.ReportParameter[] _parameters = null;

          try
          {
              _parameters = rs.GetReportParameters(_reportName, _historyID, _forRendering, _values, _credentials);
              RE2005.ExecutionInfo ei = rsExec.LoadReport(_reportName, historyID);
              RE2005.ParameterValue[] parameters = new RE2005.ParameterValue[1];

              if (_parameters.Length > 0)
              {
                  parameters[0] = new RE2005.ParameterValue();
                  parameters[0].Label = "userlogon";
                  parameters[0].Name = "userlogon";
                  parameters[0].Value = "ccastaldo";
              }
              rsExec.SetExecutionParameters(parameters, "en-us");
              results = rsExec.Render(format, deviceInfo,
               out extension, out encoding,
               out mimeType, out warnings, out streamIDs);
              using (FileStream stream = File.OpenWrite(fileName))
              {
                  stream.Write(results, 0, results.Length);
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }



      }

       public static void MergePDF(string path1,string path2,string pdfName)
      {
           using(PdfDocument coverPage = PdfReader.Open(path1,PdfDocumentOpenMode.Import))
           using(PdfDocument reportPage = PdfReader.Open(path2,PdfDocumentOpenMode.Import))
           using(PdfDocument CombinedPdf = new PdfDocument())
           {
               CopyPages(coverPage, CombinedPdf);
               CopyPages(reportPage, CombinedPdf);
               CombinedPdf.Save(pdfName);
               
           }

      }
        public static void CopyPages(PdfDocument from, PdfDocument to)
       {
           for (int i = 0; i < from.PageCount; i++)
           {
               to.AddPage(from.Pages[i]);
           }

       }
        public struct DPSEmailList
        {
            public string Name,Freq,Email,CustomerNo,PriceLevel,Email2,Fax;
            public int AttachPDF,AttachSpeadsheet,PriceChanges;
            public DateTime LastSent, SendAfter;
            public Boolean Inactive;
        }

        
    }
}
